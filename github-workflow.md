
## GitHub Workflow

![](img/github-workflow/zen.png)


---

## Within One Team

<pre><code data-line-numbers="1-400"># Checkout the master branch - you want your new branch to come from master
git checkout master

# Create a new branch named newfeature (give your branch its own simple informative name)
# Switch to your new branch
git checkout  -b newfeature

# modify code

git add .
git commit -m "new feature"

# Pull first, then rebase
git checkout master
git pull origin master
git checkout newfeature
git rebase master # or, git merge master

# push 
git push origin newfeature
</code></pre>

---
## Cross Team
#### Creating a Fork

Just head over to the GitHub page and click the "Fork" button. 
It's just that simple. Once you've done that, 
you can use clone your repo with command line:

<pre><code data-line-numbers="1-400"># Clone your fork to your local machine
git clone git@github.com:<FORKER-USERNAME>/<FORKED-PROJECT>.git
</code></pre>

---

## Cross Team
#### Create a Branch
Whenever you begin work on a new feature or bugfix, 
it's important that you create a new branch. 

<pre><code data-line-numbers="1-400"># Checkout the master branch - you want your new branch to come from master
git checkout master

# Create a new branch named newfeature (give your branch its own simple informative name)
# Switch to your new branch
git checkout  -b newfeature
</code></pre>

Now, go to town hacking away and making whatever changes you want to.

---

## Cross Team
#### Cleaning Up Your Work

<pre><code data-line-numbers="1-400"># Add 'upstream' repo to list of remotes
git remote add upstream https://github.com/<UPSTREAM-USER>/<ORIGINAL-PROJECT>.git

# Verify the new remote named 'upstream'
git remote -v

git add .
git commit -m "new feature"

# Fetch upstream master and merge with your repo's master branch (pull = fetch + merge)
git checkout master
git pull upstream master

# If there were any new commits, rebase your development branch
git checkout newfeature
git rebase master # or, git merge master
</code></pre>


---

## Cross Team
#### Submitting your Pull Request

- Once you've committed and pushed all of your changes to GitHub, 
go to the page for your fork on GitHub, select your development branch, 
and click the pull request button. 
- If you need to make any adjustments 
to your pull request, just push the updates to GitHub. Your pull request 
will automatically track the changes on your development branch and update.

---


![](img/github-workflow/forkworkflow.png)


---


## git rebase

![](img/github-workflow/git_rebase.gif)

---


## git rebase
![](img/github-workflow/gitrebase2.gif)


---

## git merge

![](img/github-workflow/gitmerge.gif)

---

## git merge

![](img/github-workflow/gitmerge2.gif)

---

## git fetch

![](img/github-workflow/gitfetch.gif)

---

## git pull

![](img/github-workflow/gitpull.gif)

---

## git conflict resolving

![](img/github-workflow/gitconflict.gif)


---

## git reset hard

![](img/github-workflow/githardreset.gif)

---

## Best Practices for Pull Requests
- Send a PR for every task
- Throw your ego in the closest garbage bin
- Take the feedback to heart
- Be strict about the temporary code
- Envision the bigger picture

Source: https://medium.com/tribalscale/requesting-sane-github-pull-requests-6f43c9cee519

---

## Best Practices to Manage Pull Request Creation and Feedback

- Create a clear PR title and description 
- Keep PRs short (same applies to files and functions)
- Manage PR disagreements through direct communication
- Avoid rewrites by getting feedback early

Source: https://doordash.engineering/2022/08/23/6-best-practices-to-manage-pull-request-creation-and-feedback/

---

## An Opinionated Guide To Handling Pull Requests
- What's in master / main Should Match What's in Production
- Alway Let People Know When Their PRs Have Been Reviewed
- When Possible, Include a GIF / Screenshot / Video of UI Changes



Source: https://www.bennadel.com/blog/4013-an-opinionated-guide-to-handling-pull-requests-prs-on-my-team.htm

---

## Git Best Practices: The Definitive Guide
- Atomic Commit
- Do not commit generated files, dependencies, local configuration files
- Test Your changes before committing, Test before you push
- VCS does not substitute for a good backup
- Enforce standards


Source: https://acompiler.com/git-best-practices/

---

## GitHub in China
- Use SSH instead of HTTPS
- It's possible to use HTTPS if you use GitHub Desktop 

---


## Thank you &nbsp;  <i class="fa-solid fa-handshake"></i>

