
## Internships in French Engineering Schools and UTSEUS

- Lunde Chen
    - Assistant professor in Computer Science, UTSEUS
    - Homepage: https://lundechen.github.io &#x1F310;	
    
---

## My Experience &nbsp;  <i class="fa-solid fa-user"></i>

- I came from École Centrale de Pékin, Beihang University &#x1F393;	
- 2011, 1 month, Shanghai Commercial Aircraft Corporation, Assistant Engineer (third year bachelor) &#x1F6EB;		
- 2013, 3 months, Guangzhou, Top Easy Inc., Android Dev (first year master) &nbsp; <i class="bi bi-android2"></i>
- 2014, 6 months, Beijing, Intel Research Center, Android Testing (second year master) &nbsp; <iconify-icon icon="file-icons:intel" style="scale:1.5"></iconify-icon>

---
## Why You Should Take an Internship (1/2) &#x1F947;		
- Expose youself to real-world experience &#x1F30D;	
- Develop new skills and increase marketability  &#x2705;
- Network with people in the industry &nbsp; <iconify-icon icon="ic:baseline-diversity-1" style="scale:1.3"></iconify-icon>
- Learn how a professional workplace operates &#x1F3E2;	
- [Build your resume](https://lundechen.github.io/#/5)  &#x1F528;	

---
## Why You Should Take an Internship (2/2) &#x1F3C6;	
- Build confidence 	&nbsp; <i class="bi bi-emoji-sunglasses"></i> 
- Get a feel for different industries &#x1F496;
- Find a role that is perfect for you &#x1F3AF;		
- Develop time management skills &#x1F553;	
- Receive helpful feedback &nbsp; <i class="bi bi-envelope-paper-heart"></i>
- Secure a job &nbsp; <i class="fa-solid fa-shield-halved"></i>	
---

## Internships at French Engineering Schools &nbsp; <iconify-icon icon="twemoji:flag-for-flag-france" style="scale:1.2"></iconify-icon> 

- Typically, two internships at Engineer Cycle &nbsp; <i class="fa-solid fa-graduation-cap"></i>
    - One for <i class="bi bi-3-circle-fill"></i> months (in the UTs, it's for <i class="bi bi-6-circle-fill"></i> months)
    - The other for <i class="bi bi-6-circle-fill"></i> months (Mission Fin d'Étude, MFE) 
- You will receive Salaries! &#x1F4B0;	
- [Example: Ecole des Ponts ParisTech](https://ecoledesponts.fr/en/advertise-internships)  &nbsp; <iconify-icon icon="mdi:france" style="scale:1.3"></iconify-icon>
- [Example: Airbus](https://www.airbus.com/en/careers/students/internship-in-france) &nbsp; <i class="fa-solid fa-plane-up"></i>

---

## Internships at UTSEUS &#x1F680;	
- Adopted from French engineering school system &#x1F495;	
- Even better than in France! &#x1F60D;	
    - UTSEUS will find an internship with you  &#x1F50D; 	 	
    - Best case scenario: for 2 years and half, you will stay at the same company, and you work there except when you have courses &#x1F917;
    - Worst case scenario: you will conduct two internships, one for 3 months, the other for 6 months, ideally at the same company
    - Your internship will be your master thesis &#x1F4C3;	
---

## It has been a while!
- [Internships at UTSEUS for 2022]()
- [Internships at UTSEUS for 2023](https://docs.qq.com/sheet/DT05zUWtnY1lPQUhS) &#x1F308;	

---




## If you prefer to read long paragraphs ... (1/3)

<small> UTSEUS’s master (engineer) program is highly innovative in that students’ industrial internships are an integral part of their master thesis research, fostering a synergy between industrial practice and scientific research. This program not only makes the internship more demanding and enriching, with students having integral, whole-scale and immersive experiences in the professional world, but also enhances the authenticity, legitimacy and applicability of the master’s thesis. Rooted in the industry, students’ academic research has also the advantage of being more innovative, more applicable in real-life settings, creating more meaningful social and practical values. In addition, companies where students conduct their internship can provide better conditions for carrying out research, reducing the overall research costs. </small> 

---


## If you prefer to read long paragraphs ... (2/3)

<small> More specifically, students’ corporate internship and master’s thesis research are combined as a whole. Students’ first 12-week (or 24-week) internship helps them to develop a deep understanding of corporate operations, industry background, and social needs from the perspective of corporate engineers. The various tasks performed by students in the enterprise help cultivate their problem-solving capabilities that engineers are expected to have in real world. Furthermore, students can discover industry-related research topics by refining problems they encounter in product design, development and production. Therefore, the master's academic research can be regarded as the continuation or deepening of the enterprise internship, and the enterprise internship is the foundation and preliminary preparation of the master's academic research. Students’ second 24-week internship is expected to be carried out in the same company or in closely-related companies, allowing them to dive more deeply in their engineering and research. Since companies are deeply involved in the entire process of UTSEUS’s master (engineer) training program, each student is assigned a corporate mentor and an academic mentor. </small> 

---


## If you prefer to read long paragraphs ... (3/3)

UTSEUS’s master (engineering) training program is also characterized by highly intensive courses, so as to ensure that students acquire a solid foundation in mathematics and science, a broad theoretical and practical knowledge and a competitive skillset. Students will also be able to develop an international vision, since a large portion of the courses are given in English and French. Moreover, students will exchange for one semester in a UT school in France. Students are expected to choose courses that are helpful for their own internship and master's research, on an individual basis.

---
## Future Directions at UTSEUS &nbsp; <i class="fa-regular fa-compass"></i>	
- Even your Bachelor Project will (and should) be conducted in companies &#x1F4AF;	
- Tight cooperations between UTSEUS and companies for training high-quality engineering students &nbsp;  <i class="fa-solid fa-user-graduate"></i>

---


## Thank You &nbsp;  <i class="fa-solid fa-handshake"></i>

