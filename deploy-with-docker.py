import time
import git
import random
import os


def git_pull_change(path):
    repo = git.Repo(path)
    current = repo.head.commit

    repo.remotes.origin.pull()

    if current == repo.head.commit:
        print("Repo not changed. Sleep mode activated.")
        return False
    else:
        print("Repo changed! Activated.")
        os.system('sudo docker cp . d6deb16d36b2:/usr/share/nginx/html/')
        return True
    
while True:
    try:
        path = '/home/lighthouse/lunde.top/'
        git_pull_change(path)
        sleep_time = max(10, random.randint(1, 50))
        time.sleep(sleep_time)
    except:
        pass