## If you are interested in joining my group as a master student

This message is sent to all my new master students:

I told and tell every new master student that here in my group we have very high standards. A minumum of 5x8=40h quality work is expected.

You are encouraged to do internships, even before our master internships. In that case  4x8=32 hours’s high-quality work would be the minimum.

With the time, according to your preferences, you will also be engaged in the research that is

- related to your master internships
- related to my research field

Be prepared that you will always have a lot of work for the next few years, with our intensive courses, two internships, and your research work.

You are also encouraged to work hard, play harder.

You should be in that mentality starting from now.

Going through this means more work, feeling less lost, less anxiety, and gain quite a lot in terms of skillset, dev/research capabilites, marketability etc.

And I can be very difficult to get along if

- minimun work requirements are not attained
- a student shows little spontaneity
- a student shows attitude issue

My general principle is that I work only with very strong students. With them around, I am a quite nice guy.

With extremely strong students, I will do my best to find very very good internships for him/her.


## Topics that interest me

### Applied Machine Learning

Applied (Evidential) ML/DL based on
Dempster-Shafer Theory.

I am especially interested in those ML/DL techniques:

- Adversarial attack
- Graph/Network related models

Visualizations.

- <https://gtvalab.github.io/projects>

## Datasets

- https://research.unsw.edu.au/projects/unsw-nb15-dataset

- https://github.com/waymo-research/waymo-open-dataset

- https://paperswithcode.com/datasets?mod=tabular

- https://paperswithcode.com/dataset/ci-mnist

- https://sebastianraschka.com/blog/2021/ml-dl-datasets.html

- https://www.unb.ca/cic/datasets/andmal2020.html

- https://zenodo.org/record/3746129

- https://datasetsearch.research.google.com/search?src=0&query=network%20intrusion&docid=L2cvMTFqY2s0OGx4Mg%3D%3D

- https://www.kaggle.com/datasets/h2020simargl/simargl2021-network-intrusion-detection-dataset

- https://www.v7labs.com/blog/best-free-datasets-for-machine-learning

## Writing papers

- https://www.overleaf.com
- https://www.tablesgenerator.com


## Quotes

The best research you can do is talk to people. <br>
-- Terry Pratchett.

Science is organized knowledge; wisdom is organized life. <br>
-- Immanuel Kant.

## Misc

- Students are encouraged to do leetcode exercises

