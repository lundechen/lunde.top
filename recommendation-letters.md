
# On the topic of Recommendation Letters 
## Lunde Chen

- I only give recommendation letters to those who I have worked with in various projects
- The bottom line is that 
    - I know you
    - You are REALLY strong
    - I know you are REALLY strong 
- There should be no exaggerative statements in the recommendation letter
    - Especially for the UT Selection Program of UTSEUS

---

## Thank you for your understanding &nbsp;  <i class="fa-solid fa-handshake"></i>

