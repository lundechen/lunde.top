
##  Join Ocademy

Content:
JupyterBook, Git/GitHub/PR, reviewNB, conda/PyEnv, RISE, GitHub Actions, 
Test-Driven Programming, 
numpy, pandas, tensorflow, pytorch, sklearn, SVM, NN, CNN, RNN, DQN, 

Webapp:
AWS amplify, nextjs, GraphQL, DynamoDB, 

UI/UX:
Figma,

商业调研：
Strategy/Marketing, 

Infrastructure:
JupyterHub, MLFlow, 