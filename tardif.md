
## ORGANISER LA FORMATION DANS UNE LOGIQUE DE PARCOURS

### L’ADN DE L’APPROCHE PAR COMPÉTENCES

- Auteur: Jacques Tardif
    - Association Française des Acteurs de l'Éducation
    - Journal: « Administration & Éducation »
    - [HomePage](https://www.uwinnipeg.ca/biology/people/faculty-staff/jacques-tardif.html)

---

## le vaste domaine de la formation

- pédagogie active
- approche par compétences
- classe inversée ou renversée
- approche-programme
- alignement pédagogique
- apprentissage signifiant
- acquis d’apprentissage (learning outcomes)
- portfolio (dossier d’apprentissage)
- évaluation authentique
- apprentissage par problèmes
- apprentissage par projets


---
## l’approche par compétences

- Le présent article conçoit l’approche par compétences comme étant un principe organisateur de la formation
- impose une logique de parcours, tant dans l’agencement des situations d’apprentissage (maquette pédagogique) que dans la progression des apprentissages de l’étudiant, et qui nécessite l’explicitation d’étapes de développement pour chacune des compétences.
---

## l’article contient quatre parties

- parcours de formation versus morcellement
- concept de compétences
- conséquences de l’adoption de l’approche par compétences
- contraintes dans l’évaluation des apprentissages

---

## parcours de formation versus morcellement
- parcours de formation
    - des formations inscrites dans une logique de parcours
- morcellement
    - des formations structurées à partir d’un découpage par disciplines


---


## morcellement

- formation de type « morcellement par disciplines » : une logique photographique
    - incite l’étudiant à concevoir que chaque cours constitue une entité indépendante, que la réussite dans un cours marque formellement la fin d’un tout, et que l’entrée dans un autre cours n’est pas nécessairement en continuité, ni en complémentarité avec un cours précédent.

---

## parcours de formation
- formation de type « parcours » : une logique vidéographique
    - du début à la fin de sa formation, chaque situation d’apprentissage soutient le développement graduel de chacune des compétences. La continuité et la complémentarité de ses apprentissages sont cruciales dans l’atteinte des finalités de la formation.


---
## Etat actuel, En milieu universitaire, 
- il est rarissime que les enseignants établissent pour l’étudiant des liens explicites entre chacune des pièces composant la maquette pédagogique
- Ce découpage par disciplines oblige notamment à déterminer des seuils de réussite (10 sur 20 par exemple) ainsi que des mécanismes de compensation
- Pour l’étudiant, cette conséquence appuie de manière significative le fractionnement des apprentissages.

---
## Le concept de compétences

- consensus sur le mot « compétence »
    - une compétence est de l’ordre de l’action
    - sa mise en œuvre repose sur la combinaison de plusieurs ressources
    - son périmètre est judicieusement circonscrit à l’aune d’un ensemble de situations
- Definition proposee par l'auteur en 2006:
    - un savoir-agir complexe reposant sur la mobilisation et la combinaison efficaces d’une variété de ressources internes et externes à l’intérieur d’une famille de situations

---

## Des conséquences de l’adoption de l’approche par compétences
- la cohérence (alignement pédagogique) dans la conception de formations inscrites dans une approche par compétences impose
    - un référentiel de compétences partagé par les formateurs
    - des étapes de développement pour chaque compétence
    - des situations d’apprentissage en continuité et en complémentarité
    - l’interdisciplinarité dans l’ensemble de la formation

---
## un référentiel de compétences partagé par les formateurs

- L’équipe enseignante doit élaborer, parfois adopter, un référentiel de compétences
- ce référentiel doit etre partagé par les formateurs


---
## des étapes de développement pour chaque compétence

- l’élaboration de la maquette pédagogique
- la planification et l’orchestration des situations d’apprentissage
- l’évaluation des apprentissages de l’étudiant tout au long de son parcours

---

## Les contraintes de l’évaluation des apprentissages (1/2)

- les seuils de passage minimaux régulièrement employés dans les formations conçues à partir d’un découpage par disciplines ne sont pas cohérents dans une approche par compétences. 
- En réalité, déterminer qu’un étudiant réussit un cours avec une note de 10/20 signifie que l’enseignant accepte que 50 % des apprentissages visés dans cette activité puissent être ignorés sans problème.
- Solution: Le savoir-agir complexe doit être régulièrement la cible de l’évaluation de la progression de l’étudiant dans son parcours de formation


---

## Les contraintes de l’évaluation des apprentissages (2/2)
- Dans l’approche par compétences, l’étudiant détient la plupart du temps la responsabilité 
    - (1) de sélectionner des preuves qui illustrent sa progression dans les étapes de développement de chaque compétence, 
    - (2) d’argumenter l’apport de chacune de ces preuves, 
    - (3) d’auto- évaluer son parcours en considérant notamment les apprentissages à réaliser dans la suite pour assurer l’atteinte des niveaux de développement subséquents

---

## Thank you &nbsp;  <i class="fa-solid fa-handshake"></i>

