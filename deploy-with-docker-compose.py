import time
import git
import random
import os


"""
How to use this script:

nohup python3 deploy-with-docker-compose.py >> ~/nohup.txt &


"""

def git_pull_change(path):
    repo = git.Repo(path)
    current = repo.head.commit

    repo.remotes.origin.pull()

    if current == repo.head.commit:
        print("Repo not changed. Sleep mode activated.")
        return False
    else:
        print("Repo changed! Activated.")
        return True
    
while True:
    try:
        path = '/home/lighthouse/lunde.top/'
        git_pull_change(path)
        sleep_time = max(10, random.randint(1, 12))
        time.sleep(sleep_time)
    except:
        pass