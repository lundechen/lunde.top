## Hi, I'm Lunde Chen

<br>

- Assistant professor in Computer Science &nbsp;<i class="fa-solid fa-laptop-code"></i>	 <br> at UTSEUS, Shanghai University

<br>
<br>

> Live, Love, Matter.

---

## More about me  &nbsp; <iconify-icon icon="streamline-emojis:magnifying-glass-tilted-left"></iconify-icon>

- [GitHub](https://github.com/reveurmichael/)  &nbsp;  <i class="fa-brands fa-github"></i>
- [Gitee](http://gitee.com/lundechen)   &nbsp;  <i class="fa-solid fa-code-branch"></i>
- [Bilibili](https://space.bilibili.com/472463946/channel/series)   &nbsp; <iconify-icon icon="simple-icons:bilibili"></iconify-icon>	
- [Google Scholar](https://scholar.google.com/citations?user=-gUgZM4AAAAJ)   &nbsp;  <iconify-icon icon="academicons:google-scholar" style="scale:1.3"></iconify-icon>

<br>
<br>

> Don't Compete. Create.

---

## Research &nbsp;  <iconify-icon icon="noto:rocket"></iconify-icon>

- **Machine Learning, Deep Learning** &nbsp; &nbsp; <iconify-icon icon="eos-icons:ai" style="scale:1.7"></iconify-icon>
	- Based on Dempster-Shafer Theory  &nbsp; <i class="fa-solid fa-atom"></i>
- **Applied Machine Learning** &nbsp; <iconify-icon icon="bi:robot"></iconify-icon>
	- Medical imaging &nbsp; <i class="fa-solid fa-circle-radiation"></i>

<br>

> It is through science that we prove, <br> but through intuition that we discover. <br>
> -- Henri Poincaré

---

## Teaching &nbsp; <iconify-icon icon="game-icons:teacher"></iconify-icon>

- **C++ Design Patterns** (FR, undergraduate)
- **Machine Learning** (EN, undergraduate)
- **Data Engineering** (EN, postgraduate)

<br>
<br>

> Tell me and I forget. <br>Teach me and I remember. <br>Involve me and I learn. <br>
> -- Benjamin Franklin

---

## Education &nbsp; <i class="fa-solid fa-graduation-cap"></i>

- **BS and MS** 
	- Beihang University, China 
- **Ph.D in Computer Science**
	- Toulouse University, France

---

## Skills	&nbsp;  <iconify-icon icon="emojione-v1:hammer" style="scale:1.3"></iconify-icon>

- **Programming** &nbsp; <i class="fa-solid fa-keyboard"></i>
	- Python, Java, C/C++, PHP, <span class="latex">L<sup>A</sup>T<sub>E</sub>X</span>, JavaScript, HTML, CSS, R, Matlab/Scilab
- **Network** &nbsp; <iconify-icon icon="zondicons:network"></iconify-icon>
	- Software-Defined Network (SDN)
	- Wireless networks
- **Machine Learning, Deep Learning** &nbsp; <iconify-icon icon="ant-design:dot-chart-outlined" style="scale:1.6"></iconify-icon>
- **Miscellaneous**   &nbsp; <iconify-icon icon="fluent-emoji-flat:rainbow" style="scale:1.1"></iconify-icon>
	- Docker, Hadoop, Linux kernel programming, graph theory, combinatorial optimization, CPLEX
---

##  Languages &nbsp; <iconify-icon icon="emojione-v1:globe-showing-europe-africa" style="scale:1.3"></iconify-icon>
- <span><b>Chinese  &nbsp;</b> <span> <img style="width:1em; height:1em; display: inline; position: relative; top: 0.6em" src="img/index/china.png" /> </span> </span>
	- Native language
- <span><b>English  &nbsp;</b> <span> <img style="width:1em; height:1em; display: inline; position: relative; top: 0.6em" src="img/index/united-states.png" /> </span> </span>
	- Professional proficiency
- <span><b>French  &nbsp;</b> <span> <img style="width:1em; height:1em; display: inline; position: relative; top: 0.6em" src="img/index/france.png" /> </span> </span>
	- Professional proficiency


---

## Interests &nbsp; <i class="fa-solid fa-heart" style="color:red"></i>

- Books <iconify-icon icon="noto-v1:books"></iconify-icon>, podcasts <i class="fa-solid fa-podcast"></i>, audiobooks <iconify-icon icon="icomoon-free:headphones"></iconify-icon>
- Jogging <iconify-icon icon="fluent-emoji-flat:man-running"></iconify-icon>, bicycling <iconify-icon icon="fluent-emoji-flat:person-mountain-biking-light"></iconify-icon>	
- Ping pong &#x1F3D3;, swimming <iconify-icon icon="noto-v1:person-swimming"></iconify-icon>
- Meditation <iconify-icon icon="mdi:meditation" style="scale:1.3"></iconify-icon>, mindfulness <iconify-icon icon="emojione-v1:milky-way" style="scale:1.1"></iconify-icon>
<br>
<br>

> You should sit in meditation for twenty minutes every day - unless you're too busy; then you should sit for an hour.

---

## Food &nbsp; 	<iconify-icon icon="dashicons:food" style="scale:1.3"></iconify-icon>

- I am a semi-vegetarian &#x1F955;  &#x1F33D;	 &#x1F952;	&#127834; &#x1F346;	&#x1F345;	&#x1F34E;	&#x1F34C;	&#x1F347;	&#x1F348;	&#x1F34A;	&#129370; &#x1F34F;	&#x1F350; &#x1F954;	 &#129371;	&#x1F33E;	 &#129382; &#x1F351;	&#x1F352;	&#x1F95D;	&#x1F951; &#129388; &#127812; &#127838; &#129360; 	&#x1F957; &#129389; &#127821; 


<br>
<br>

> You are what you eat.


---


## <span> Ocademy &nbsp; <span> <img style="width:1em; position: relative; top: 0.45em; scale:1;" src="img/index/logo.png" /> </span>  </span>

- An online learning platform
- https://ocademy-ai.github.io/machine-learning

<br>
<br>

> Welcome to the future.


---



## Contact &nbsp; <iconify-icon icon="bytesize:telephone" style="scale:1.1"></iconify-icon>
- WeChat: `CLD_michael`  &nbsp; <i class="fa-brands fa-weixin"></i> 
- Email: lundechen@shu.edu.cn   &nbsp; <i class="fa-regular fa-envelope"></i>

<br>
<br>



> Grant me the strength to focus this week, to be mindful and present, to serve with excellence, to be a force of love.

---

## Thank you &nbsp;  <i class="fa-solid fa-handshake"></i>

<br>

> Love people, <br>
> Use things,  <br>
> Because the opposite never works.

