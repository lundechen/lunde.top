# About

http://lunde.top website, hosted on Tencent Singapore VM. 

```
ssh lighthouse@43.142.12.204
```

## Best revealjs plugin

https://github.com/Martinomagnifico/reveal.js-appearance

https://martinomagnifico.github.io/

https://rogeralmeida.github.io/revealjs-animated-examples/#/

https://github.com/rogeralmeida/revealjs-animated


https://rajgoel.github.io/reveal.js-demos/


## GitPython

```
sudo apt install python3-pip
pip install GitPython
```

https://gitpython.readthedocs.io/en/stable/quickstart.html


## Docker Option 1

```
sudo docker build -t imagea .
sudo nohup docker run -it --rm -d -p 80:80 --name containera imagea > ~/nohup.txt &
sudo docker ps
sudo docker cp . 887547b76118:/usr/share/nginx/html/
```


## Docker Option 2

```
sudo docker run --name mynginxcontainer --mount type=bind,source=/home/lighthouse/lunde.top,target=/usr/shar
e/nginx/html,readonly -p 8064:80 -d nginx
```


## Docker Compose
```
sudo docker-compose up -d
```


## Python periodic_deploy.py

```
nohup python periodic_deploy.py >> ~/nohup.txt &
```

## HTML Shapes

- https://albertwalicki.com/blog/creating-shapes-in-css
- https://www.w3schools.com/howto/howto_css_shapes.asp
- https://css-tricks.com/the-shapes-of-css/

## HTML Emoji

https://www.quackit.com/character_sets/emoji/emoji_v3.0/unicode_emoji_v3.0_characters_all.cfm

https://www.freecodecamp.org/news/all-emojis-emoji-list-for-copy-and-paste/

https://www.w3schools.com/charsets/ref_utf_misc_symbols.asp

## CSS

https://icon-sets.iconify.design/ (Used in this project)

https://www.w3schools.com/icons/tryit.asp?filename=tryicons_fa-github


https://freefrontend.com/css-animated-backgrounds/

https://www.phoca.cz/cssflags/

https://mdbootstrap.com/docs/standard/content-styles/flags/

https://alexsobolenko.github.io/flag-icons/

https://brettsnaidero.github.io/svg-animation-slides/#/39


https://primer.style/octicons/archive-16


https://ionic.io/ionicons

https://fonts.google.com/icons

https://icons.getbootstrap.com/

## Revealjs Themes

https://visualcomputing.github.io/colors/#/themes

## Revealjs Background

https://revealjs.com/backgrounds/

## Reveal.js-Title-Footer

https://github.com/e-gor/Reveal.js-Title-Footer

http://e-gor.github.io/Reveal.js-Title-Footer/demo

## reveal-mark-plugin

https://github.com/stlab/reveal-mark-plugin

## ELAPSED-TIME-BAR PLUGIN

https://tkrkt.github.io/reveal.js-elapsed-time-bar/#/



## Server Side Speaker Notes

https://github.com/reveal/notes-server


## TODO

Google Scholar Icon

https://jpswalsh.github.io/academicons/

https://github.com/Viglino/iconicss

https://github.com/erikflowers/weather-icons

https://github.com/lipis/flag-icons

https://saeedalipoor.github.io/icono/

https://github.com/wentin/cssicon


https://thesabbir.github.io/simple-line-icons/

https://tabler-icons.io/


## Effects

https://tympanus.net/Development/IconHoverEffects/


https://tympanus.net/Development/CreativeButtons/

https://tympanus.net/Development/CreativeLinkEffects/


## Nginx remove .html extension

https://stackoverflow.com/questions/38228393/nginx-remove-html-extension



## Marked.js

https://javascript.plainenglish.io/revolutionise-the-way-you-write-docs-with-marked-js-5d1de8b8e725

