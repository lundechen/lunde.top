
# If you wanna join my group as a master student
## Lunde Chen

This message is sent to all my new master students:

I told and tell every new master student that here in my group we have very high standards. A minumum of 5x8=40h quality work is expected.

You are encouraged to do internships, even before our master internships. In that case  4x8=32 hours’s high-quality work would be the minimum.

With the time, according to your preferences, you will also be engaged in the research that is

&nbsp;&nbsp;&nbsp;&nbsp;-  related to your master internships

&nbsp;&nbsp;&nbsp;&nbsp;-  related to my research field

Be prepared that you will always have a lot of work for the next few years, with our intensive courses, two internships, and your research work.

You are also encouraged to work hard, play harder.

You should be in that mentality starting from now.

Going through this means more work, feeling less lost, less anxiety, and gain quite a lot in terms of skillset, dev/research capabilites, marketability etc.

And I can be very difficult to get along if

&nbsp;&nbsp;&nbsp;&nbsp; - minimun work requirements are not attained

&nbsp;&nbsp;&nbsp;&nbsp;- a student shows little spontaneity

&nbsp;&nbsp;&nbsp;&nbsp;-  a student shows attitude issue

My general principle is that I work only with very strong students. With them around, I am a quite nice guy.

With extremely strong students, I will do my best to find very very good internships for him/her.
